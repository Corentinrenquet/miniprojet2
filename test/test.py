import unittest
import programme
import csv
import os


class miniproj2test(unittest.TestCase):
    def testlire_entree(self):
        fichier = open(".\auto.csv", newline='')
        file_input = csv.DictReader(fichier, delimiter='|')
        buffer = file_input
        self.assertEqual(programme.lire_entree(file_input), buffer)
        fichier.close()

    def testcreer_sortie(self):
        fichier = open(".\auto.csv", newline='')
        file_input = csv.DictReader(fichier, delimiter='|')
        buffer = file_input

        sortie = open("./testsortie.txt",newline='')
        self.assertEqual(programme.gestion_sortie(buffer),sortie)


# déposer le dépôt le 25/10/19
# on peut commit pendant toutes les vacances
