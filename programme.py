import csv
import os
 
buffer=[]
 
 
def gestion_entree():
    file_input=input("Saisir le nom du fichier CSV d'entrée : ")
    if(os.path.isfile(file_input)==True):
        lire_entree(file_input)
    else:
        print("Erreur: fichier introuvable")
        exit(1)
 
 
def gestion_sortie():
    file_output=input("Saisir le nom du fichier CSV de sortie : ")
    creer_sortie(file_output)
 
 
 
def lire_entree(file_input):
    with open(file_input,newline='') as csvfile:
        lecteur = csv.DictReader(csvfile, delimiter='|')
        for line_content in lecteur:
            buffer.append(line_content)
 
 
 
 
def creer_sortie(file_output):
    with open(file_output,'w',newline='') as csvfile:
        field_names=[
            'adresse_titulaire','nom','prenom','immatriculation','date_immatriculation','vin','marque','denomination_commeciale','couleur',
            'carroserie','catégorie','cylindree','energie','places','poids','puissance','type','variante','version'
            
            
            
                    ]
        copieur = csv.DictWriter(csvfile, field_names,delimiter=";")
        copieur.writeheader()
        for row in buffer:
            liste_caract=row['type_variante_version'].split(', ')
            copieur.writerow({
                'adresse_titulaire': row['address'],
                'nom': row['name'],
                'prenom': row['firstname'],
                'immatriculation': row['immat'],
                'date_immatriculation': row['date_immat'],
                'vin': row['vin'],
                'marque': row['marque'],
                'denomination_commeciale': row['denomination'],
                'couleur': row['couleur'],
                'carroserie': row['carrosserie'],
                'catégorie': row['categorie'],           
                'cylindree': row['cylindree'],
                'energie': row['energy'],
                'places': row['places'],
                'poids': row['poids'],
                'puissance': row['puissance'],
                'type': liste_caract[0],
                'variante': liste_caract[1],
                'version': liste_caract[2],
                
                            })
 
 #|cylindree|date_immat|denomination|energy|firstname|immat|marque|name|places|poids|puissance|type_variante_version|vin
if __name__ == '__main__':
    gestion_entree()
    gestion_sortie()
    exit(0)
